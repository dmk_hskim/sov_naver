import os
import sys
import urllib
import pprint
import requests
import json

config = open('config.json', encoding='UTF-8')
config_dict = json.load(config)

CLIENT_ID = config_dict.get('naver_ad_api_id')
CLIENT_SECRET = config_dict.get('naver_ad_api_secret')
CHANNEL='blog'
#channel=['blog','cafearticle','news','kin']

def _url(channel,keyword):
    encText = urllib.parse.quote_plus(keyword)
    url = "https://openapi.naver.com/v1/search/{0}?query={1}&display=20".format(channel,encText)
    return url

def _fetch(keyword):
    e=requests.session()
    e.headers["X-Naver-Client-Id"] = CLIENT_ID
    e.headers["X-Naver-Client-Secret"] = CLIENT_SECRET
    api = e.get(_url(CHANNEL,keyword))
    json=api.json()
    item = _parse(json)
    return item

def _parse(json):
    items=[]
    for i in range(len(json['items'])):
        title=json['items'][i]['title'].replace('<b>','').replace('</b>','')
        content=json['items'][i]['description'].replace('<b>','').replace('</b>','')
        url=json['items'][i]['link']

        item={
            'title':title,
            'content':content,
            'url':url,
            'channel':CHANNEL
        }
        items.append(item)
    return items


if __name__ == "__main__":
    pprint.pprint(_fetch("냉장고"))

