import requests
from bs4 import BeautifulSoup
import time
import urllib.parse
import re
import datetime


class Crawler():
    def crawl(self, params):
        keyword = params['keyword']

        items=[]
        for page in range(1,3):
            url = self._url(keyword,page)
            results = self._fetch(url)

            for r in results:
                items.append(r)

        return items


    def _url(self, keyword, page):
        keyword_co = urllib.parse.quote_plus(keyword, safe='', encoding='utf-8')
        url = "https://search.naver.com/search.naver?where=article&sm=tab_jum&query={0}&start={1}".format(keyword_co,page)
        return url

    def _fetch(self, url):
        e = requests.session()
        e.headers['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'
        res = requests.get(url)
        soup = BeautifulSoup(res.text,'html.parser')

        items = self._parse(soup)
        return items

    def _parse(self, soup):
        items = []
        now = datetime.datetime.now()
        for item in soup.select('ul.type01 > li'):
            title = item.select('dl>dt')[0]
            title = title.get_text().replace('\n', '').replace('\t', '').replace('\r', '')

            content = item.select('dl>dd')[1]
            content = content.get_text()

            url = item.select('li>dl>dt>a')[0]
            url = url['href']

            item = {
                'channel': 'navercafe',
                'keyword': params['keyword'],
                # 'rank': rank,
                'title': title,
                'content': content,
                'url': url,
                'time': now.strftime('%Y-%m-%d %H:%M')
            }
            items.append(item)

        return items

if __name__=="__main__":
    params={'keyword':'화웨이'}
    a=Crawler()
    for i in a.crawl(params):
        print(i)