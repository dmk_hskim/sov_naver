import requests
from bs4 import BeautifulSoup
import datetime
import time
import urllib.parse
import re
import datetime
from pandas import DataFrame
import psycopg2 as pg2
import sqlalchemy
from sqlalchemy import create_engine

# naver powerlink
def naver_powerlink_data(keyword):
    keywords = keyword
    keyword = urllib.parse.quote_plus(keyword, safe='', encoding='utf-8')
    url = 'https://ad.search.naver.com/search.naver?where=ad&query={0}'.format(keyword)
    e = requests.session()
    e.headers['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'
    res = requests.get(url)
    soup = BeautifulSoup(res.text, 'html.parser')
    items = []
    now = datetime.datetime.now()
    for power in soup.select('ol > li'):
        rank = ""

        title = power.select('a.lnk_tit')[0]
        title = title.get_text()

        content = power.select('p.ad_dsc')[0]
        content = content.get_text().replace('\n', '').replace("  ","")

        url = power.select('a.url')[0]
        url = url.get_text()

        item = {
            'time':now.strftime('%Y-%m-%d %H:%M'),
            'channel':'powerlink',
            'keyword': keywords,
            #'rank': rank,
            'title': title,
            'content': content,
            'url': url
        }
        items.append(item)

    return items[:20]


# naver shopping

def naver_shopping_data(keyword):
    keywords = keyword
    keyword = urllib.parse.quote_plus(keyword, safe='', encoding='utf-8')
    url = 'http://shopping.naver.com/search/all.nhn?query={0}'.format(keyword)
    e = requests.session()
    e.headers[
        'User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'
    res = requests.get(url)
    soup = BeautifulSoup(res.text, 'html.parser')
    items = []
    now = datetime.datetime.now()
    for shopping in soup.select('ul.goods_list > li'):
        rank = ""

        title = shopping.select('div.info > a.tit')[0]
        title = title.get_text().replace('\n', '').replace('\t', '').replace('  ','')

        content = ""

        url = shopping.select('div.info a.tit')[0]
        url = url['href']

        item = {
            'time':now.strftime('%Y-%m-%d %H:%M'),
            'channel':'navershopping',
            'keyword': keywords,
            #'rank': rank,
            'title': title,
            'content': content,
            'url': url
        }
        items.append(item)

    return items[:20]


## post

def naver_post_data(keyword):
    keywords = keyword
    keyword = urllib.parse.quote_plus(keyword, safe='', encoding='utf-8')
    url = 'http://m.post.naver.com/search/post.nhn?keyword={0}'.format(keyword)
    e = requests.session()
    e.headers[
        'User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'
    res = requests.get(url)
    soup = BeautifulSoup(res.text, 'html.parser')
    items = []
    now = datetime.datetime.now()
    for post in soup.select('ul.lst_feed li'):
        rank = ""

        title = post.select('strong')[0]
        title = title.get_text().replace('\n', '').replace('\t', '').replace('\r', '')

        content = post.select('p.text_feed.ell')[0]
        content = content.get_text()

        url = post.select('div.text_area > a.link_end')[0]
        url = "http://m.post.naver.com" + url['href'].replace('&searchRank.*', '')

        item = {
            'time':now.strftime('%Y-%m-%d %H:%M'),
            'channel':'post',
            'keyword': keywords,
            #'rank': rank,
            'title': title,
            'content': content,
            'url': url
        }
        items.append(item)

    return items


# categorys = ['article', 'news', 'post', 'kin']
def naver_cbnk_data(keyword, category, strpage):
    # cafe,blog,news,kin
    if category == 'article':
        channel = 'cafe'
    elif category == 'news':
        channel = 'news'
    elif category == 'post':
        channel = 'blog'
    elif category == 'kin':
        channel = 'kin'
    keywords=keyword
    strpage = str(strpage)
    keyword = urllib.parse.quote_plus(keyword, safe='', encoding='utf-8')
    url = "https://search.naver.com/search.naver?where=" + category + "&sm=tab_jum&query=" + keyword + "&start=" + strpage
    e = requests.session()
    e.headers[
        'User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'
    res = requests.get(url)
    soup = BeautifulSoup(res.text, 'html.parser')
    items = []
    now = datetime.datetime.now()
    for item in soup.select('ul.type01 > li'):

        title = item.select('dl>dt')[0]
        title = title.get_text().replace('\n', '').replace('\t', '').replace('\r', '')

        content = item.select('dl>dd')[1]
        content = content.get_text()

        url = item.select('li>dl>dt>a')[0]
        url = url['href']
        item = {
            'time':now.strftime('%Y-%m-%d %H:%M'),
            'channel': channel,
            'keyword':keywords,
            #'rank': rank,
            'title': title,
            'content': content,
            'url': url
        }
        items.append(item)

    return items


if __name__ == '__main__':
    keyword = '냉장고'
    now = datetime.datetime.now()
    power=naver_powerlink_data(keyword)
    time.sleep(2.2)
    shopping=naver_shopping_data(keyword)
    time.sleep(1.8)
    post=naver_post_data(keyword)
    time.sleep(2.9)

    categorys = ['article', 'news', 'post', 'kin']
    blog, cafe, news, kin = [], [], [], []
    for strpage in [1, 11]:
        time.sleep(13.438)
        for item in categorys:
            channel = ""
            if item == 'article':
                cafe += naver_cbnk_data(keyword, item, strpage)
            elif item == 'post':
                blog += naver_cbnk_data(keyword, item, strpage)
            elif item == 'news':
                news += naver_cbnk_data(keyword, item, strpage)
            elif item == 'kin':
                kin += naver_cbnk_data(keyword, item, strpage)

    engine = create_engine('postgresql://postgres:1234@192.168.0.12/dpitest')

    df_shopping=DataFrame.from_records(shopping)
    df_cafe=DataFrame.from_records(cafe)
    df_blog=DataFrame.from_records(blog)
    df_news=DataFrame.from_records(news)
    df_kin=DataFrame.from_records(kin)
    df_post=DataFrame.from_records(post)
    df_power=DataFrame.from_records(power)
    
    df_shopping.to_sql(name='sov_test', con=engine, if_exists = 'append', index=False)
    df_cafe.to_sql(name='sov_test', con=engine, if_exists = 'append', index=False)
    df_blog.to_sql(name='sov_test', con=engine, if_exists = 'append', index=False)
    df_news.to_sql(name='sov_test', con=engine, if_exists = 'append', index=False)
    df_kin.to_sql(name='sov_test', con=engine, if_exists = 'append', index=False)
    df_post.to_sql(name='sov_test', con=engine, if_exists = 'append', index=False)
    df_power.to_sql(name='sov_test', con=engine, if_exists = 'append', index=False)
