import requests
from bs4 import BeautifulSoup
import time
import urllib.parse
import re
import datetime


class Crawler():
    def crawl(self, params):
        keyword = params['keyword']
        url = self._url(keyword)

        items = self._fetch(url)

        return items


    def _url(self, keyword):
        keyword_co = urllib.parse.quote_plus(keyword, safe='', encoding='utf-8')
        url = 'https://ad.search.naver.com/search.naver?where=ad&query={0}'.format(keyword)
        return url

    def _fetch(self, url):
        e = requests.session()
        e.headers['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'
        res = requests.get(url)
        soup = BeautifulSoup(res.text,'html.parser')

        items = self._parse(soup)
        return items

    def _parse(self, soup):
        items = []
        now = datetime.datetime.now()
        for power in soup.select('ol > li'):
            rank = ""

            title = power.select('a.lnk_tit')[0]
            title = title.get_text()

            content = power.select('p.ad_dsc')[0]
            content = content.get_text().replace('\n', '').replace("  ", "")

            url = power.select('a.url')[0]
            url = url.get_text()

            item = {
                'channel': 'powerlink',
                'keyword': params['keyword'],
                # 'rank': rank,
                'title': title,
                'content': content,
                'url': url,
                'time': now.strftime('%Y-%m-%d %H:%M')
            }
            items.append(item)

        return items[:20]

if __name__=="__main__":
    params={'keyword':'화웨이'}
    a=Crawler()
    for i in a.crawl(params):
        print(i)