import requests
from bs4 import BeautifulSoup
import time
import urllib.parse
import re
import datetime


class Crawler():
    def crawl(self, params):
        keyword = params['keyword']
        url = self._url(keyword)

        items = self._fetch(url)

        return items


    def _url(self, keyword):
        keyword_co = urllib.parse.quote_plus(keyword, safe='', encoding='utf-8')
        url = 'https://search.naver.com/search.naver?sm=top_hty&fbm=1&ie=utf8&query={0}'.format(keyword_co)
        return url

    def _fetch(self, url):
        res = requests.get(url)
        soup = BeautifulSoup(res.text,'html.parser')

        items = self._parse(soup)
        return items

    def _parse(self, soup):
        now = datetime.datetime.now()
        items = []
        rank = 0
        for rel in soup.select('dd.lst_relate._related_keyword_list> ul > li'):
            rank += 1
            rel_keyword = rel.get_text().replace(" ", "")

            item = {
                'rank': rank,
                'keyword': params['keyword'],
                'rel_keyword': rel_keyword,
                'time': now.strftime('%Y-%m-%d %H:%M')
            }
            items.append(item)

        return items