import pandas as pd
from pandas import DataFrame
import psycopg2 as pg2
import sqlalchemy
from sqlalchemy import create_engine
engine = create_engine('postgresql://postgres:1234@192.168.0.12/dpitest')

"""
def cal_list_sov(channel_list, sov_object):
    title_content = []
    count = 0
    for i in channel_list:
        title_content.append(i['title'] + i['content'])
    for title_contents in title_content:
        if sov_object in title_contents:
            count += 1
    sov = count / len(channel_list)

    return sov
def cal_df_sov(df, sov_object):
    title_content = []
    count = 0
    for i in range(len(df)):
        title_content.append(df['title'][i] + df['content'][i])
    for title_contents in title_content:
        if sov_object in title_contents:
            count += 1
    sov = count / len(df)

    return sov
"""

def cal_list_sov(channel_list, sov_object_lists):
    title_content = []
    count = 0
    for i in channel_list:
        title_content.append(i['title'] + i['content'])

    for title_contents in title_content:
        for sov_object_list in sov_object_lists:
            if sov_object_list in title_contents:
                count += 1
                break
    sov = count / len(channel_list)

    return sov


def cal_df_sov(df, sov_object_lists):
    title_content = []
    count = 0
    for i in range(len(df)):
        title_content.append(df['title'][i] + df['content'][i])

    for title_contents in title_content:
        for sov_object_list in sov_object_lists:
            if sov_object_list in title_contents:
                count += 1
                break
    sov = count / len(df)

    return sov

def view_sov(keyword,sov_object):
    sov = {
        'keyword': keyword,
        'company': sov_object,
        'powerlink': cal_df_sov(df_powerlink, sov_object),
        'shopping': cal_df_sov(df_shopping, sov_object),
        'cafe': cal_df_sov(df_cafe, sov_object),
        'kin': cal_df_sov(df_kin, sov_object),
        'blog': cal_df_sov(df_blog, sov_object),
        'news': cal_df_sov(df_news, sov_object),
        'post': cal_df_sov(df_post, sov_object)
    }
    return sov




if __name__ == '__main__':
    keyword = '에어컨'
    sov_object = ['LG전자','LG','휘센']

    df_cafe = pd.read_sql("select * from sov_test where channel='cafe'and keyword='{0}'; ".format(keyword), engine)
    df_blog = pd.read_sql("select * from sov_test where channel='blog'and keyword='{0}'; ".format(keyword), engine)
    df_news = pd.read_sql("select * from sov_test where channel='news'and keyword='{0}'; ".format(keyword), engine)
    df_shopping = pd.read_sql("select * from sov_test where channel='navershopping'and keyword='{0}'; ".format(keyword),engine)
    df_kin = pd.read_sql("select * from sov_test where channel='kin'and keyword='{0}'; ".format(keyword), engine)
    df_post = pd.read_sql("select * from sov_test where channel='post'and keyword='{0}'; ".format(keyword), engine)
    df_powerlink = pd.read_sql("select * from sov_test where channel='powerlink'and keyword='{0}'; ".format(keyword),
                               engine)

    sov=view_sov(keyword, sov_object)
    print(sov)



    """
    df_post=pd.read_sql("select * from sov_test where channel='post'and keyword='{0}'; ".format(keyword), engine)
    print(cal_df_sov(df_post, sov_object))
    """